#include "../includes/parsing.h"

void parse_data(char *file_name, Student_t *students_array, short *students_num) {
    FILE *fp = fopen(file_name, "r"); // open file
    fseek(fp, 0, SEEK_END); // get size
    long size = ftell(fp);
    rewind(fp);
    char *data = (char *) malloc(sizeof(char) * (size + 1));
    fread(data, sizeof(char), size, fp); // reading
    data[size] = '\0';
    fclose(fp);

    cJSON *json = cJSON_Parse(data);
    cJSON *json_students = cJSON_GetObjectItem(json, "students");
    cJSON *json_this_student_p;

    short counter = 0;
    cJSON_ArrayForEach(json_this_student_p, json_students) {
        write_student(json_this_student_p, &students_array[counter]);
        students_array[counter].in_stack = 0;
        counter++;
    }
    *students_num = counter;
}



void write_student(cJSON *json_student_p, Student_t *student_p) {
    // get data about current student
    cJSON *json_id = cJSON_GetObjectItem(json_student_p, "student_id");
    cJSON *json_name = cJSON_GetObjectItem(json_student_p, "student_name");
    cJSON *json_record_number = cJSON_GetObjectItem(json_student_p, "student_record_number");
    cJSON *json_rating = cJSON_GetObjectItem(json_student_p, "student_rating");
    cJSON *json_attendance = cJSON_GetObjectItem(json_student_p, "student_attendance");
    cJSON *json_login = cJSON_GetObjectItem(json_student_p, "student_login");

    // convert data to necessary type and write them
    student_p->id = json_id->valueint;
    student_p->name = json_name->valuestring;
    student_p->record_number = json_record_number->valueint;
    student_p->rating = json_rating->valuedouble;
    student_p->attendance = json_attendance->valueint;
    student_p->login = json_login->valuestring;
}