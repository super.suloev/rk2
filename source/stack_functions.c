#include "../includes/stack_functions.h"


void init_and_create_stack(Student_t *students_array, short students_num) {
    Stack_t stack_p;
    stack_p.head_p = NULL;

    short input_id;
    while (!all_students_in_stack(students_array, students_num)) {
        printf("Enter the student id: \t");
        scanf("%hd", &input_id);
        push(students_array, &stack_p, input_id, students_num);
    }

    printf("INFORMATION IN STACK: \n\n");
    pop_all_and_print(&stack_p);
}



short all_students_in_stack(Student_t *students_array, short students_num) {
    for (short i = 0; i < students_num; ++i) {
        if (students_array[i].in_stack == 0)
            return 0;
    }
    return 1;
}

void push(Student_t *student_array, Stack_t *stack_p, short student_id, short students_num) {
    short pushed = 0;
    for (short i = 0; i < students_num; ++i)
        if (student_array[i].id == student_id) {
            pushed = 1;
            student_array[i].in_stack = 1;
            Node_t *node_p;
            node_p = (Node_t*) malloc(sizeof(Node_t));
            node_p->student_p = &student_array[i];
            node_p->next_student_p = stack_p->head_p;
            stack_p->head_p = node_p;
        }

    if (!pushed)
        printf("It is no student with id %d \n", student_id);
}



void pop_all_and_print(Stack_t *stack_p) {
    while (stack_p->head_p != NULL) {
        pop_and_print_one(stack_p);
    }
}


void pop_and_print_one(Stack_t *stack_p) {
    Node_t *this_node_p;
    this_node_p = stack_p->head_p;
    stack_p->head_p = this_node_p->next_student_p;

    print_student(this_node_p->student_p);
    free(this_node_p);
}