#include "../includes/printing.h"

void print_data(Student_t *students_array, short students_num) {
    printf("Information about students:\n\n");
    for (short i = 0; i < students_num; ++i) {
        print_student(&students_array[i]);
    }
}


void print_student(Student_t *student_p) {
    printf("id: %d \t name: %s \n", student_p->id, student_p->name);
    printf("record_number: %d \n", student_p->record_number);
    printf("rating: %.2f \n", student_p->rating);
    printf("attendance: %d \n", student_p->attendance);
    printf("login: %s \n\n\n", student_p->login);
}

