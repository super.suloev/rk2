#include "includes/main.h"

int main(int argc, char *argv[]) {
    Student_t students_array[STUDENTS_MAX_NUM];
    short students_num;
    parse_data(argv[1], students_array, &students_num);
    print_data(students_array, students_num);

    init_and_create_stack(students_array, students_num);
    return 0;
}
