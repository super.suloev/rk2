#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../cJSON/cJSON.h"
#include "data.h"

#ifndef BC_3_PARSING_H
#define BC_3_PARSING_H

void parse_data(char *file_name, Student_t *students_array, short *students_num);
void write_student(cJSON *json_student_p, Student_t *student_p);

#endif
