#ifndef BC_3_DATA_H
#define BC_3_DATA_H

#define STUDENTS_MAX_NUM 50

// information about student
typedef struct {
    short id, in_stack;
    char *name, *login;
    int record_number, attendance;
    double rating;
} Student_t;

// stack

typedef struct Node_t {
    Student_t *student_p;
    struct Node_t *next_student_p;
} Node_t;

typedef struct {
    Node_t *head_p;
} Stack_t;


#endif
