#ifndef BC_3_STACK_FUNCTIONS_H
#define BC_3_STACK_FUNCTIONS_H

#include <stdio.h>
#include <stdlib.h>
#include "data.h"
#include "printing.h"

void init_and_create_stack(Student_t *students_array, short students_num);
short all_students_in_stack(Student_t *students_array, short students_num);
void push(Student_t *student_array, Stack_t *stack_p, short student_id, short students_num);
void pop_all_and_print(Stack_t *stack_p);
void pop_and_print_one(Stack_t *stack_p);



#endif
