#ifndef BC_3_PRINTING_H
#define BC_3_PRINTING_H

#include <stdio.h>
#include "data.h"

void print_data(Student_t *students_array, short students_num);
void print_student(Student_t *student_p);

#endif
